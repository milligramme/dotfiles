set nocompatible
set number
set ruler

syntax on
set cursorline
set nocursorcolumn

" hide invisible
set nolist

set tabstop=2
set expandtab
set autoindent

set showmatch
set wrapscan
set hlsearch

set history=10000

