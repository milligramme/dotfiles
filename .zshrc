# sh config

# ====================================================
# == aliases
# ====================================================

# shell
alias reload="source ~/.zshrc"

# finder
alias o="open ."

# textmate
alias m="mate ."
alias a="atom ."

# bundler
alias be="bundle exec"
alias bi="bundle install"
alias bu="bundle update"

# diff --unified
alias diff="colordiff -u"

# tree -N mojibake fix
alias tree="tree -N"

# grep
alias grep="grep --color=auto"

# peco
alias upfiles="git tag | peco | xargs git diff --name-only"
alias repo="ghq list --full-path | peco | xargs mate"


# ====================================================
# == config
# ====================================================

# zsh hisotry
export HISTFILE=${HOME}/.zsh_history
export HISTSIZE=1000
export SAVEHIST=100000
setopt hist_ignore_dups
setopt EXTENDED_HISTORY

# ls
export CLICOLOR=1
export LSCOLORS=DxGxcxdxCxegedabagacad

#homebrew
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:$PATH
export HOMEBREW_GITHUB_API_TOKEN=bdc21fdca2816e765bfaa33f826484149a85fd97
export HOMEBREW_INSTALL_BADGE="👹"

# zsh-completions via homebrew
fpath=($(brew --prefix)/share/zsh-completions $fpath)

# zsh_completion
autoload -U compinit; compinit -u
autoload -U colors; colors
zstyle ':completion:*:default' menu select=1
zstyle ':completion:*:sudo:*' command-path /usr/local/sbin /usr/local/bin \
                             /usr/sbin /usr/bin /sbin /bin /usr/X11R6/bin \
                             /usr/local/git/bin

# rbenv
# Fix zsh rbenv.bash problem by matthias-guenther Âˇ Pull Request #251 Âˇ sstephenson/rbenv https://github.com/sstephenson/rbenv/pull/251
SHELL="/bin/zsh"
export PATH="$HOME/.rbenv/bin:$PATH"
eval "$(rbenv init -)"

# nodebrew
export PATH=$HOME/.nodebrew/current/bin:$PATH
export NODEBREW_ROOT=$HOME/.nodebrew

# gem
export GEM_EDITOR='mate'


# ====================================================
# == function
# ====================================================

# # show git branch
# https://gist.github.com/uasi/214109
function git-current-branch {
  local name st color
  name=$(basename "`git symbolic-ref -q HEAD 2> /dev/null`")
  st=`git status 2> /dev/null`

  if [[ -z $name && -n `echo "$st" | grep "^HEAD detached at"` ]]; then
    name="(detached)"
  elif [[ -z $name ]]; then
    return
  fi

  if [[ -n `echo "$st" | grep "^nothing to commit"` ]]; then
    color=${fg[blue]}
  elif [[ -n `echo "$st" | grep "^nothing added to commit but"` ]]; then
    color=${fg[yellow]}
  elif [[ -n `echo "$st" | grep "^You have unmerged paths."` ]]; then
    color=${fg[red]}
  elif [[ -n `echo "$st" | grep "^HEAD detached at"` ]]; then
    color=${fg_bold[yellow]}
  elif [[ -n `echo "$st" | grep "^Untracked files:"` ]]; then
    color=${bg_bold[red]}
  elif [[ -n `echo "$st" | grep "^Changes not staged for commit"` ]]; then
    color=${bg_bold[cyan]}
  elif [[ -n `echo "$st" | grep "^Changes to be committed"` ]]; then
    color=${bg_bold[green]}
  else
    color=${fg[white]}
  fi

  echo %{$color%}\[$name\]%{$reset_color%}
}

# function wifi-speed {
#   linkspeed=`/System/Library/PrivateFrameworks/Apple80211.framework/Versions/A/Resources/airport -I | awk '/lastTxRate/{print($2)}'`
#   if [[ ${linkspeed} != "" ]]; then
#     #statements
#     echo ${linkspeed}Mbps
#   else
#     echo OFFLINE
#   fi
# }

# color
# black red green yellow blue magenta cyan white

setopt prompt_subst
PROMPT='%F{green}%~ %f`git-current-branch`%b [%D %T] %# '
# RPROMPT='[%D %T `wifi-speed`]'

function ghq-update(){
  ghq list | sed -E 's/^[^\/]+\/(.+)/\1/' | xargs -n 1 -P 10 ghq get -u
}


